# angularproject

1. Download the zip file from the link
https://gitlab.com/s2pedutech/angularproject.git

2. Unzip and extract in some folder (second)

3. open that folder in command prompt. (That folder should contain the package.json file)

4. npm install 
    This command will download all the dependencies required

5. ng serve 

6. Open this url:
    http://localhost:4200

